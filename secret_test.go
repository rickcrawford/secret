package secret

import (
	"crypto/rand"
	"fmt"
	"io"
	"testing"
)

func TestCrypto(t *testing.T) {

	key := make([]byte, 32)
	io.ReadFull(rand.Reader, key[:])
	val := []byte("this is a test")

	encryptedVal, err := Encrypt(key, val)
	if err != nil {
		t.Fatalf("error encrypting: %s", err)
	}

	decryptedVal, err := Decrypt(key, encryptedVal)
	if err != nil {
		t.Fatalf("error decrypting: %s", err)
	}

	fmt.Printf("Encrypted: %s\n", string(encryptedVal))
	fmt.Printf("Decrypted: %s\n", string(decryptedVal))
}

func TestBase64Crypto(t *testing.T) {

	key := make([]byte, 32)
	io.ReadFull(rand.Reader, key[:])
	val := "this is a test"

	encryptedVal, err := EncryptToBase64String(string(key), val)
	if err != nil {
		t.Fatalf("error encrypting: %s", err)
	}

	decryptedVal, err := DecryptFromBase64String(string(key), encryptedVal)
	if err != nil {
		t.Fatalf("error decrypting: %s", err)
	}

	fmt.Printf("Encrypted: %s\n", encryptedVal)
	fmt.Printf("Decrypted: %s\n", decryptedVal)
}
